package backend.classes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import backend.classes.Common;
import backend.database.Database;

public class Skill implements Common {
	public String skill;
	public int sid;
	
	public Skill(String skill, int sid) {
		this.skill = skill;
		this.sid = sid;
	}
	
	public boolean isNotFound() { return false; }
	
	public static ArrayList<Skill> getAllSkills() {
		try {
			ResultSet rs = Database.executeQuery("select skill, sid from skills");
			ArrayList<Skill> skills = new ArrayList<Skill>();
			while(rs.next()) {
				String sname = rs.getString(1);
				int sid = rs.getInt(2);				
				Skill skill = new Skill(sname, sid);				
				skills.add(skill);
			}
			return skills;
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			return new ArrayList<Skill>();
		}
	}
}
