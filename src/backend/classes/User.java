package backend.classes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import backend.classes.Common;
import backend.classes.Notification;
import backend.classes.UnauthorizedError;
import backend.database.Database;


public class User implements Common {
	public String username;
	public String email;
	public String location;
	public String resumeLink;
	public String moreLinks;
	public String imagePath;
	public String name;
	
	public ArrayList<Project> owner;
	public ArrayList<Project> part;
	
	private transient boolean isAuthenticated = false;
	
	public static User NOT_FOUND = new User("", "", "", "", "", "", "");
	
	public User(String name, String username, String email, String location, String resumeLink, String moreLinks, String imagePath) {
		this.name = name;
		this.username = username;
		this.email = email;
		this.location = location;
		this.resumeLink = resumeLink;
		this.moreLinks = moreLinks;
		this.imagePath = imagePath;
	}
	
	public User(String username) {
		this.username = username;
	}
	
	public boolean isNotFound() {
		return this == NOT_FOUND;
	}
	
	public boolean authenticate(String password) {
		String query = "select password from users where username=?";
		try {
			ResultSet rs = Database.executeQuery(query, this.username);
			boolean hasNext = rs.next();
			if (hasNext) {
				String ogpwd = rs.getString(1);
				this.isAuthenticated = true;
				return password.equals(ogpwd);
			}
			else {
				return false;
			}
		}
		catch (SQLException ex ) {
			return false;
		}
	}
	
	public static User getByUsername(String username) {
		String query = "select username, name, email, location, resume_link, more_links, image_path from users where username=?";
		try {
			ResultSet rs = Database.executeQuery(query, username);
			boolean hasNext = rs.next();
			if (hasNext) {
				String name = rs.getString(2);
				String email = rs.getString(3);
				String location = rs.getString(4);
				String resumeLink = rs.getString(5);
				String moreLinks = rs.getString(6);
				String imagePath = rs.getString(7);
				return new User(name, username, email, location, resumeLink, moreLinks, imagePath);
			}
			else {
				return NOT_FOUND;
			}
			
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			return NOT_FOUND;
		}
	}
	
	public ArrayList<Notification> notifications() throws UnauthorizedError {
		if (this.isAuthenticated) {
			return Notification.getUserNotifications(this.username);
		}
		else {
			throw new UnauthorizedError();
		}
	}
	
	public static User register(String name, String username, String email, String password, int[] aids, int[] sids) {
		try {
			Database.setAutoCommit(false);
			
			String userInsertionQuery = "insert into users(name, username, password, email) values(?, ?, ?, ?)";
			Database.executeUpdate(userInsertionQuery, name, username, password, email);
			
			String skillsInsertionQuery = "insert into user_skills(username, sid) values(?, ?)";
			for(int i = 0; i < sids.length; i++) 
				Database.executeUpdate(skillsInsertionQuery, username, sids[i]);
			
			String areasInsertionQuery = "insert into user_aois(username, aid) values(?, ?)";
			for(int i = 0; i < aids.length; i++) 
				Database.executeUpdate(areasInsertionQuery, username, aids[i]);			

			Database.commit();
			Database.setAutoCommit(true);
			
			User currUser = new User(username);
			currUser.isAuthenticated = true;
			return currUser;
		}
		catch (SQLException ex) {
			try {
				Database.rollback();				
			}
			catch (SQLException exc) {
				
			}
			return null;
		}
	}
	
	public boolean changePassword(String newPassword) throws UnauthorizedError {
		if (this.isAuthenticated) {
			String query = "update users set password=? where username=?";
			try {
				Database.executeUpdate(query, newPassword, this.username);
				return true;
			}
			catch (SQLException ex) {
				return false;
			}
		}
		else {
			throw new UnauthorizedError();
		}
	}
	
	public boolean reportSpam(String otherUser) throws UnauthorizedError {
		if (this.isAuthenticated) {
			try {
				String query = "insert into user_spams(reporter, spammer) values(?, ?)";
				Database.executeQuery(query, this.username, otherUser);
				return true;
			}
			catch(SQLException ex) {
				return false;
			}			
		}
		else {
			throw new UnauthorizedError();
		}
	}
	
	public boolean reportSpam(int pid) throws UnauthorizedError {
		if (this.isAuthenticated) {
			try {
				String query = "insert into project_spams(reporter, pid) values(?, ?)";
				Database.executeQuery(query, this.username, pid);
				return true;
			}
			catch(SQLException ex) {
				return false;
			}			
		}
		else {
			throw new UnauthorizedError();
		}
	}

	public boolean changeLiking(int pid, boolean like) throws UnauthorizedError {
		if (this.isAuthenticated) {
			try {
				ResultSet rs = Database.executeQuery("select * from project_likings where pid=? and username=?", pid, this.username);
				boolean hasALiking = rs.next();
				
				if (hasALiking) {
					String query = "update project_likings set likeDislike=? where pid=? and username=?";
					Database.executeUpdate(query, like, pid, this.username);
				}
				else {
					String query = "insert into project_likings(pid, username, likeDislike) values(?, ?, ?)";
					Database.executeUpdate(query, pid, this.username, like);
				}
				return true;
			}
			catch(SQLException ex) {
				return false;
			}
		}
		else {
			throw new UnauthorizedError();
		}
	}
	
	public Project createProject(String title, String description, int[] aids, int[] sids) throws UnauthorizedError {
		if (this.isAuthenticated) {
			Project proj = Project.create(title, description, this.username, aids, sids);
			return proj;
		}
		else {
			throw new UnauthorizedError();
		}
	}
	
	public boolean act(String user, int pid, boolean accept) throws UnauthorizedError {
		if (Project.getProjectById(pid).equals(this.username)) {
			try {
				Database.executeUpdate("update project_requests set status=? where pid=? and from=?", accept, pid, user);				
				return true;
			}
			catch(SQLException ex) {
				return false;
			}			
		}
		else {
			throw new UnauthorizedError();
		}
	}

	public ArrayList<Project> projects() {
		try {
			String query = "select title, description, pid, numberOfLikes, numberOfDislikes from projects where owner=?";
			ResultSet rs = Database.executeQuery(query, this.username);

			ArrayList<Project> projects = new ArrayList<Project>();

			while(rs.next()) {
				String title = rs.getString(1);
				String description = rs.getString(2);
				int pid = rs.getInt(3);
				int numberOfLikes = rs.getInt(4);
				int numberOfDislikes = rs.getInt(5);
				Project project = new Project(title, description, this.username, pid, numberOfLikes, numberOfDislikes);
				projects.add(project);
			}
			this.owner = projects;
			return projects;
		}
		catch(SQLException ex) {
			ex.printStackTrace();
			return new ArrayList<Project>();
		}
	}

	public ArrayList<Project> partOf() {
		try {
			String query = "select pid from project_members where username=?";
			ResultSet rs = Database.executeQuery(query, this.username);

			ArrayList<Project> projects = new ArrayList<Project>();

			while(rs.next()) {
				int pid = rs.getInt(1);
				Project project = Project.getProjectById(pid);
				projects.add(project);
			}
			this.part = projects;
			return projects;
		}
		catch(SQLException ex) {
			ex.printStackTrace();
			return new ArrayList<Project>();
		}
	}
	
	public String toString() {
		return String.format("<User (username=%s, )>", this.username);
	}
	
	public ArrayList<Area> areas() throws UnauthorizedError {
		if (this.isAuthenticated) {
			try {
				ResultSet rs = Database.executeQuery("select aid from user_aois where username=?", this.username);
				
				ArrayList<Area> areas = new ArrayList<Area>();
				
				while(rs.next()) {
					int aid = rs.getInt(1);
					ResultSet rst = Database.executeQuery("select area from areas where aid=?", aid);
					rst.next();
					String area = rst.getString(1);
					areas.add(new Area(area, aid));
				}
				return areas;
			}
			catch (SQLException ex) {
				ex.printStackTrace();
				return null;
			}
		}
		else {
			throw new UnauthorizedError();
		}
	}
	
	public ArrayList<Skill> skills() throws UnauthorizedError {
		if (this.isAuthenticated) {
			try {
				ResultSet rs = Database.executeQuery("select sid from user_skills where username=?", this.username);
				
				ArrayList<Skill> skills = new ArrayList<Skill>();
				
				while(rs.next()) {
					int sid = rs.getInt(1);
					ResultSet rst = Database.executeQuery("select skill from skills where sid=?", sid);
					rst.next();
					String skill = rst.getString(1);
					skills.add(new Skill(skill, sid));
				}
				return skills;
			}
			catch (SQLException ex) {
				ex.printStackTrace();
				return null;
			}
		}
		else {
			throw new UnauthorizedError();
		}
	}
	
	public ArrayList<Project> getFeed() throws UnauthorizedError {
		if(this.isAuthenticated) {
			try {
				ArrayList<Area> areas = this.areas();
				ArrayList<Skill> skills = this.skills();

				ArrayList<Project> projects = new ArrayList<Project>();
				
				String query;
				ResultSet rs;
				String clause = "";
				
				if (skills.size() > 0) {
					clause += String.format("sid=%d", skills.get(0).sid);
					
					for(int i = 0; i < skills.size(); i++) {
						clause += String.format("or sid=%d", skills.get(i).sid);
					}
					
					query = "select pid from project_skills where " + clause;
					rs = Database.executeQuery(query);
					
					while(rs.next()) {
						int pid = rs.getInt(1);
						Project project = Project.getProjectById(pid);
						projects.add(project);
					}					
				}
				

				if (areas.size() > 0) {
					clause = String.format("aid=%d", areas.get(0).aid);
					
					for(int i = 0; i < areas.size(); i++) {
						clause += String.format("or aid=%d", areas.get(i).aid);
					}
					
					query = "select pid from project_areas where " + clause;
					rs = Database.executeQuery(query);
					
					while(rs.next()) {
						int pid = rs.getInt(1);
						Project project = Project.getProjectById(pid);
						projects.add(project);
					}					
				}
				
				return projects;				
			}
			catch(SQLException ex) {
				ex.printStackTrace();
				return null;
			}
		}
		else {
			throw new UnauthorizedError();
		}
	}
}
